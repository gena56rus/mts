﻿using System.Linq;
using System;
using System.Collections.Generic;

namespace IJustHaveToAsk
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var array = new[] { 1, 2, 3, 4 }.EnumerateFromTail(2);

            foreach ((int item, int? tail) in array)
            {
                Console.WriteLine($"{item}, {tail}");
            }
        }

        /// <summary>
        /// <para> Отсчитать несколько элементов с конца </para>
        /// <example> new[] {1,2,3,4}.EnumerateFromTail(2) = (1, ), (2, ), (3, 1), (4, 0)</example>
        /// </summary> 
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="tailLength">Сколько элеметнов отсчитать с конца  (у последнего элемента tail = 0)</param>
        /// <returns></returns>
        public static IEnumerable<(T item, int? tail)> EnumerateFromTail<T>(this IEnumerable<T> enumerable, int? tailLength)
        {
            var result = new List<(T item, int? tail)>();
            int count =  enumerable.Count();
            int i = 0;

            foreach (T value in enumerable)
            {
                if (i <= count - tailLength - 1)
                {
                    result.Add((value, null));
                }
                else
                {
                    tailLength--;
                    result.Add((value, tailLength));
                }
                i++;
            }

            return result;
        }
    }
}
