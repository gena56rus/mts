﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace TopGrade
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var sort = Sort(new int[] { 11, 22, 33, 455, 644, 21, 32, 0, 2, 4 },
                            11,
                            644);


            foreach (int value in sort)
            {
                Console.WriteLine(value);
            }

        }

        /// <summary>
        /// Возвращает отсортированный по возрастанию поток чисел
        /// </summary>
        /// <param name="inputStream">Поток чисел от 0 до maxValue. Длина потока не превышает миллиарда чисел.</param>
        /// <param name="sortFactor">Фактор упорядоченности потока. Неотрицательное число. Если в потоке встретилось число x, 
        /// то в нём больше не встретятся числа меньше, чем (x - sortFactor).</param>
        /// <param name="maxValue">Максимально возможное значение чисел в потоке. Неотрицательное число, не превышающее 2000.</param>
        /// <returns>Отсортированный по возрастанию поток чисел.</returns>
        public static IEnumerable<int> Sort(IEnumerable<int> inputStream, int sortFactor, int maxValue)
        {
            var sort = new List<int>();
            var min = inputStream.Min();
            
            while (min < maxValue)
            {
                sort.Add(min);
                inputStream = inputStream.Where(x => x >= min + sortFactor);
                min = inputStream.Min();
            }

            sort.Add(maxValue);
            return sort;
        }

    }
}
